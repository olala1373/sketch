package com.example.mohammadreza.sketch;

import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.mohammadreza.sketch.data.FilesContract.FilesEntry;
import com.example.mohammadreza.sketch.data.FolderContract;
import com.example.mohammadreza.sketch.filesactivities.NotepadActivity;
import com.example.mohammadreza.sketch.filesactivities.PaintActivity;

import java.io.File;
import java.io.FileInputStream;

public class FilesActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener {

    private long folderId;
    private static final int FILE_LOADER = 0;
    FilesAdapter filesAdapter;
    private long file_Id;
    private Intent intent;
    private Intent goIntent;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    private boolean isFabOpen = false;
    private FloatingActionButton fab , fab_note , fab_paint;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_files);
         intent = getIntent();
        Uri uri = intent.getData();
         folderId = Long.valueOf(uri.getLastPathSegment());
        setTitle(getFolderName(folderId));



        initialize();
        fab.setOnClickListener(this);
        fab_note.setOnClickListener(this);
        fab_paint.setOnClickListener(this);

        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v("BUTTON CLICKED" , "im clicking");
                //animateFab();
               // addFilesDialog();
            }

        });*/

        displayTableInfo();
    }

    public void initialize(){
        goIntent = new Intent(FilesActivity.this , NotepadActivity.class);
        /**
         * initialize fabs
         */
        fab = (FloatingActionButton) findViewById(R.id.add_files_button);
        fab_note = (FloatingActionButton) findViewById(R.id.add_notes_button);
        fab_paint = (FloatingActionButton) findViewById(R.id.add_paint_button);
        /**
         * initialize animations
         */
        fab_open = AnimationUtils.loadAnimation(this , R.anim.fab_open);
        rotate_forward = AnimationUtils.loadAnimation(this , R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(this , R.anim.rotate_backward);
        fab_close = AnimationUtils.loadAnimation(this , R.anim.fab_close);
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        Uri folderUri;
        switch (id){
            case R.id.add_files_button:
                animateFab();
                Log.v("BUTTON CLICKED" , "im clicking");
                break;
            case R.id.add_notes_button:
                folderUri = ContentUris.withAppendedId(FolderContract.FolderEntry.CONTENT_URI, folderId);
                goIntent.setData(folderUri);
                startActivity(goIntent);
                break;
            case R.id.add_paint_button:
                Intent intent = new Intent(FilesActivity.this , PaintActivity.class);
                folderUri = ContentUris.withAppendedId(FolderContract.FolderEntry.CONTENT_URI, folderId);
                intent.setData(folderUri);
                startActivityForResult(intent , 1);
                break;
        }
    }

    public void animateFab(){

        if(isFabOpen){

            fab.startAnimation(rotate_backward);
            fab_note.startAnimation(fab_close);
            fab_paint.startAnimation(fab_close);
            fab_note.setClickable(false);
            fab_paint.setClickable(false);
            isFabOpen = false;
        }
        else {
            fab.startAnimation(rotate_forward);
            fab_note.startAnimation(fab_open);
            fab_paint.startAnimation(fab_open);
            fab.setRippleColor(getResources().getColor(R.color.gery_icon));
            fab_note.setClickable(true);
            fab_paint.setClickable(true);
            isFabOpen = true;

        }

    }


    @Override
    protected void onRestart(){
        super.onRestart();

        displayTableInfo();
    }



    //creating a dialog for adding files
   /* public void addFilesDialog(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
      *//**  LayoutInflater inflater = this.getLayoutInflater();
        final View root = inflater.inflate(R.layout.addfiles_dialog, null);
        alertDialogBuilder.setView( root);**//*

      alertDialogBuilder.setTitle("Choose:");
      alertDialogBuilder.setPositiveButton("note", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
              Uri folderUri = ContentUris.withAppendedId(FolderContract.FolderEntry.CONTENT_URI, folderId);
              goIntent.setData(folderUri);
              startActivity(goIntent);
          }
      });

        alertDialogBuilder.setNegativeButton("paint", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(FilesActivity.this , PaintActivity.class);
                Uri folderUri = ContentUris.withAppendedId(FolderContract.FolderEntry.CONTENT_URI, folderId);
                intent.setData(folderUri);
                startActivityForResult(intent , 1);
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }*/



    public void displayTableInfo(){

      /*  String[] projection = {
                FilesEntry.COLUMN_FOLDER_ID,
                FilesEntry.COLUMN_FILE_NAME,
                FilesEntry.COLUMN_TITLE,
                FilesEntry.COLUMN_CONTENT,
                FilesEntry.COLUMN_CONTENT,
                FilesEntry.COLUMN_IMG_URL,
                FilesEntry.COLUMN_FOLDER_ID
        };*/
        String[] projection = {
                FilesEntry._ID,
                FilesEntry.COLUMN_FILE_NAME,
                FilesEntry.COLUMN_ISNOTE
        };

        String whereClause = String.format("%s IN (%d)" , FilesEntry.COLUMN_FOLDER_ID , folderId);

        final Cursor cursor = getContentResolver().query(FilesEntry.CONTENT_URI ,
                projection ,
                whereClause ,
                null,
                null);

        filesAdapter = new FilesAdapter(this , cursor , true);
        ListView listView =  (ListView) findViewById(R.id.main_files_list);
        listView.setAdapter(filesAdapter);
        registerForContextMenu(listView);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Uri uri = ContentUris.withAppendedId(FilesEntry.CONTENT_URI , l);
                file_Id = Long.valueOf(uri.getLastPathSegment());
                int isnote = cursor.getInt(cursor.getColumnIndex("isNote"));
                if(isnote == 1){
                    editNote(file_Id);
                }
                else {
                    viewPaint(file_Id);
                }
            }
        });
       // getSupportLoaderManager().initLoader(FILE_LOADER , null , FilesActivity.this);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        Uri uri = ContentUris.withAppendedId(FilesEntry.CONTENT_URI , info.id);
        file_Id = Long.valueOf(uri.getLastPathSegment());
        menu.add(0 , v.getId() , 0 , "Delete");
        menu.add(0,v.getId(),0,"Edit");
    }
    public boolean onContextItemSelected(MenuItem item){

            String[] projection = {
                FilesEntry.COLUMN_FOLDER_ID,
                FilesEntry.COLUMN_FILE_NAME,
                FilesEntry.COLUMN_TITLE,
                FilesEntry.COLUMN_CONTENT,
                FilesEntry.COLUMN_CONTENT,
                FilesEntry.COLUMN_IMG_URL,
                FilesEntry.COLUMN_FOLDER_ID
        };

        switch (item.getTitle().toString()){
            case "Edit":
                Toast.makeText(getApplicationContext(), "Edit Clicked", Toast.LENGTH_LONG).show();
                break;

            case "Delete":
                Uri uri = Uri.parse(FilesEntry.CONTENT_URI + "/" + file_Id);
                int rows = getContentResolver().delete(
                        uri,
                        FilesEntry._ID + " '=" + file_Id + " '" ,
                        null
                        );
                Toast.makeText(getApplicationContext(), "Deleted :" + rows, Toast.LENGTH_LONG).show();

                break;
        }

        return true;
    }

    public void editNote(long file_Id){
        goIntent.putExtra("file_id" , "" + file_Id);
        startActivity(goIntent);
    }

    public void viewPaint(long file_Id){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View root = inflater.inflate(R.layout.paint_image_view , null);
        alertDialogBuilder.setView(root);
        File file = new File(getImage((int)file_Id));
        try {
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(file));
            ImageView imageView = root.findViewById(R.id.paint_imageview);
            imageView.setImageBitmap(b);
        }
        catch (Exception e){

        }
        alertDialogBuilder.create().show();
    }

    public String getImage(int fileid){
        String[] projection = {
          FilesEntry._ID ,
                FilesEntry.COLUMN_FILE_NAME,
                FilesEntry.COLUMN_IMG_URL
        };

        String whereClause = String.format(" %s IN (%d)" , FilesEntry._ID , fileid);

        Cursor cursor = getContentResolver().query(FilesEntry.CONTENT_URI , projection , whereClause , null , null);
        if (cursor.moveToFirst())
        return cursor.getString(cursor.getColumnIndex(FilesEntry.COLUMN_IMG_URL));
        else
            return null;
    }

    //Get a folder name and set it to activity title
    public String getFolderName(long id){
        String[] projection = {

                FolderContract.FolderEntry.COLUMN_FOLDER_NAME
        };

        Cursor cursor = getContentResolver().query(
                FolderContract.FolderEntry.CONTENT_URI,
                projection,
                FolderContract.FolderEntry._ID + "='" + id + "'",
                null,
                null
        );
        String name = "null";
        if(cursor.moveToFirst()){
            name = cursor.getString(cursor.getColumnIndex("name"));
        }
        return name;
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {
                FilesEntry._ID,
                FilesEntry.COLUMN_FILE_NAME,
        };
        return new CursorLoader(this,
                FilesEntry.CONTENT_URI,
                projection,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        filesAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        filesAdapter.swapCursor(null);
    }

}
