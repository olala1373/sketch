package com.example.mohammadreza.sketch.filesactivities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.mohammadreza.sketch.R;

/**
 * Created by mohammadreza on 8/25/2017.
 */

public class GridviewAdapter extends BaseAdapter {

    private Context c;
    private Integer[] images;

    public GridviewAdapter(Context c , Integer[] images){
        this.c = c;
        this.images = images;
    }
    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int i) {
        return images[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertview, ViewGroup parent) {
        if(convertview == null){
            LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertview = inflater.inflate(R.layout.gridview_layout , null);

        }

        ImageView imageView = convertview.findViewById(R.id.color_image);
        imageView.setImageResource(images[i]);

        return convertview;
    }
}
