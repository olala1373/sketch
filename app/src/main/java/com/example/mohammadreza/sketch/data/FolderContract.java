package com.example.mohammadreza.sketch.data;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by mohammadreza on 8/21/2017.
 */

public final class FolderContract  {

    public static String AUTHORITY = "com.example.mohammadreza.sketch";
    public static String PATH_FOLDERS = "folder";
    public static final Uri BASE_URI = Uri.parse("content://com.example.mohammadreza.sketch");


    private FolderContract(){}

    public static final class FolderEntry implements BaseColumns{
        public final static String TABLE_NAME ="folder";
        //columns in table
        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_FOLDER_NAME = "name";

        public final static Uri CONTENT_URI = Uri.withAppendedPath(BASE_URI , PATH_FOLDERS);

    }

}
