package com.example.mohammadreza.sketch.filesactivities;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mohammadreza.sketch.R;
import com.example.mohammadreza.sketch.data.FilesContract.FilesEntry;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PaintActivity extends AppCompatActivity {


    private Integer[] colorPicker = {
            R.drawable.black,
            R.drawable.blue,
            R.drawable.gray,
            R.drawable.green,
            R.drawable.orange,
            R.drawable.pink,
            R.drawable.red_circle,
            R.drawable.light_blue
    };


    private GridView gridView;

    private long folder_id;
    private String title = null;
    private String content = null;

    DrawingView drawingView;
    View rootview;
    Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint);
        setTitle(R.string.paint_name);
        rootview = findViewById(R.id.draw_paint);
        rootview.setDrawingCacheEnabled(true);

        Intent intent = getIntent();
        Uri uri = intent.getData();
        folder_id = Long.valueOf(uri.getLastPathSegment());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_option_paint, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()){
            case R.id.action_edit:
                edittextDialog();
                break;

            case R.id.action_save:
                saveDialog();

                break;
        }


        return super.onOptionsItemSelected(item);
    }

    public void saveDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View root = inflater.inflate(R.layout.save_dialog , null);




        alertDialogBuilder.setTitle("Enter file name:");


        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EditText name_Edittext = root.findViewById(R.id.notepad_name);
                String name = name_Edittext.getText().toString();

                bitmap = Bitmap.createBitmap(rootview.getDrawingCache());
                ContextWrapper cw = new ContextWrapper(getApplicationContext());
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                String imgname = "" + name + ".png";
                File mypath=new File(directory,imgname);
                FileOutputStream fos = null;


                try{
                    fos = new FileOutputStream(mypath);
                    bitmap.compress(Bitmap.CompressFormat.PNG , 100 , fos);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                insertPaint(name , mypath.toString());

                Intent returnIntent = new Intent();
                returnIntent.putExtra("result","false");
                setResult(Activity.RESULT_OK,returnIntent);
                finish();


            }
        });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertDialogBuilder.setView(root);
        alertDialogBuilder.create().show();

    }

    public void insertPaint(String name , String img_url){
        ContentValues values = new ContentValues();
        values.put(FilesEntry.COLUMN_FILE_NAME , name);
        values.put(FilesEntry.COLUMN_TITLE , title);
        values.put(FilesEntry.COLUMN_CONTENT , content);
        values.put(FilesEntry.COLUMN_IMG_URL , img_url);
        values.put(FilesEntry.COLUMN_FOLDER_ID , folder_id);
        values.put(FilesEntry.COLUMN_ISNOTE , 0);

        Uri uri = getContentResolver().insert(FilesEntry.CONTENT_URI , values);

    }


    public void edittextDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View root = inflater.inflate(R.layout.edit_notepad_text, null);
        TextView brush = root.findViewById(R.id.editor_seekbar_name);
        brush.setText("Brush size");
        gridView = (GridView) root.findViewById(R.id.gridview);
        GridviewAdapter gridviewAdapter = new GridviewAdapter(this , colorPicker);
        alertDialogBuilder.setView( root);
        gridView.setAdapter(gridviewAdapter);
        final DrawingView drawingView = new DrawingView(PaintActivity.this , null);
        /**
         * Clicklistener on colors
         */
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        drawingView.setColor(getResources().getColor(R.color.black_icon));
                        break;
                    case 1:
                        drawingView.setColor(getResources().getColor(R.color.blue_icon));
                        break;
                    case 2:
                        drawingView.setColor(getResources().getColor(R.color.gery_icon));
                        break;
                    case 3:
                        drawingView.setColor(getResources().getColor(R.color.green_icon));
                        break;
                    case 4:
                        drawingView.setColor(getResources().getColor(R.color.orange_icon));
                        break;
                    case 5:
                        drawingView.setColor(getResources().getColor(R.color.pink_icon));
                        break;
                    case 6:
                        drawingView.setColor(getResources().getColor(R.color.red_icon));
                        break;
                    case 7:
                        drawingView.setColor(getResources().getColor(R.color.light_blue_icon));
                        break;

                }
            }
        });
        /**
         * End of click listener
         */

        alertDialogBuilder.setTitle("Text Editor");

        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertDialogBuilder.create().show();

    }
}
