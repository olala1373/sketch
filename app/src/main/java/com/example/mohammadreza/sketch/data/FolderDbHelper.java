package com.example.mohammadreza.sketch.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.mohammadreza.sketch.data.FolderContract.FolderEntry;
import com.example.mohammadreza.sketch.data.FilesContract.FilesEntry;
/**
 * Created by mohammadreza on 8/21/2017.
 */

public class FolderDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "sketch.db";

    private static final int DATABASE_VERSION = 6;

    private static final String CREATE_FILE_TABLE = String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT , " +
                    "%s TEXT NOT NULL , %s TEXT , %s TEXT , %s TEXT , %s INTEGER , %s INTEGER NOT NULL ,FOREIGN KEY (%s) REFERENCES folder(%s)" +
                    " ON DELETE CASCADE); " ,
            FilesEntry.TABLE_NAME ,
            FilesEntry._ID ,
            FilesEntry.COLUMN_FILE_NAME,
            FilesEntry.COLUMN_TITLE,
            FilesEntry.COLUMN_CONTENT,
            FilesEntry.COLUMN_IMG_URL,
            FilesEntry.COLUMN_FOLDER_ID,
            FilesEntry.COLUMN_ISNOTE,
            FilesEntry.COLUMN_FOLDER_ID,
            FolderEntry._ID);

    public FolderDbHelper (Context context){
        super(context , DATABASE_NAME , null , DATABASE_VERSION);

    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        //CREATE TABLE folder (id integer primary key autoincrement , name text not null);
        String createFolderTable = "CREATE TABLE " + FolderEntry.TABLE_NAME
                + "(" + FolderEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                +FolderEntry.COLUMN_FOLDER_NAME + " TEXT NOT NULL);";
        db.execSQL(createFolderTable);

        db.execSQL(CREATE_FILE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + FilesEntry.TABLE_NAME);
        db.execSQL(CREATE_FILE_TABLE);

    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.execSQL("PRAGMA foreign_keys=ON");
    }
}
