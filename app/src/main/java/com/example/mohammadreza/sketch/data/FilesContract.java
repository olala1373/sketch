package com.example.mohammadreza.sketch.data;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by mohammadreza on 8/23/2017.
 */

public final class FilesContract {
    public static String AUTHORITY = "com.example.mohammadreza.sketch";
    public static String PATH_FOLDERS = "file";
    public static final Uri BASE_URI = Uri.parse("content://com.example.mohammadreza.sketch");

    private FilesContract(){}

    public static final class FilesEntry implements BaseColumns{

        public final static String TABLE_NAME ="file";
        //columns in table
        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_FILE_NAME = "name";
        public final static String COLUMN_TITLE = "title";
        public final static String COLUMN_CONTENT = "content";
        public final static String COLUMN_FOLDER_ID = "folder_Id";
        public final static String COLUMN_IMG_URL = "imageURL";
        public final static String COLUMN_ISNOTE = "isNote";

        //Uri of my files
        public final static Uri CONTENT_URI = Uri.withAppendedPath(BASE_URI , PATH_FOLDERS);

    }
}
