package com.example.mohammadreza.sketch.filesactivities;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ViewStubCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mohammadreza.sketch.FilesActivity;
import com.example.mohammadreza.sketch.MainActivity;
import com.example.mohammadreza.sketch.R;
import com.example.mohammadreza.sketch.data.FilesContract;
import com.example.mohammadreza.sketch.data.FolderContract;

public class NotepadActivity extends AppCompatActivity {

    private Integer[] colorPicker = {
            R.drawable.black,
            R.drawable.blue,
            R.drawable.gray,
            R.drawable.green,
            R.drawable.orange,
            R.drawable.pink,
            R.drawable.red_circle,
            R.drawable.light_blue
    };


    private GridView gridView;
    private long folderId;
    private String name;
    private String title;
    private String content;
    private String image_Url = null;
    private int file_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notepad);
        setTitle(R.string.notepad_name);

        Intent intent = getIntent();
        Uri uri = intent.getData();
        if(uri != null){
            folderId = Long.valueOf(uri.getLastPathSegment());
        }
        else {
            String fileid = intent.getExtras().getString("file_id");
             file_id = Integer.parseInt(fileid);
            setNote(file_id);
        }

        //Toast.makeText(this , "this is folder :" + folderId + "file :" + file_id , Toast.LENGTH_SHORT).show();

    }

    public void setNote(int file_id){
        String[] projection = {
                FilesContract.FilesEntry._ID,
                FilesContract.FilesEntry.COLUMN_FILE_NAME,
                FilesContract.FilesEntry.COLUMN_TITLE,
                FilesContract.FilesEntry.COLUMN_CONTENT
        };

        String whereClause = String.format("%s IN (%d)" , FilesContract.FilesEntry._ID , file_id);

        Cursor cursor = getContentResolver().query(FilesContract.FilesEntry.CONTENT_URI,
                projection,
                whereClause,
                null,
                null);

        String note_name = null;
        if(cursor.moveToFirst()){
            name = cursor.getString(cursor.getColumnIndex("name"));
            title = cursor.getString(cursor.getColumnIndex("title"));
            content = cursor.getString(cursor.getColumnIndex("content"));
        }

        setTitle(name);
        EditText editText = (EditText) findViewById(R.id.notepad_title);
        EditText editText1 = (EditText) findViewById(R.id.notepad_content);
        editText.setText(title);
        editText1.setText(content);

    }

    public void updateDialog(final int id){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View root = inflater.inflate(R.layout.save_dialog , null);

        EditText title_Edittext = (EditText) findViewById(R.id.notepad_title);
        EditText content_Edittext = (EditText) findViewById(R.id.notepad_content);


        alertDialogBuilder.setTitle("Enter file name:");
        title = title_Edittext.getText().toString();

        content = content_Edittext.getText().toString();

        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EditText name_Edittext = root.findViewById(R.id.notepad_name);
                name = name_Edittext.getText().toString();

                updateNote(name , title , content , id);
                finish();

            }
        });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertDialogBuilder.setView(root);
        alertDialogBuilder.create().show();
    }

    public void updateNotewoDialog(final int id){
        EditText title_Edittext = (EditText) findViewById(R.id.notepad_title);
        EditText content_Edittext = (EditText) findViewById(R.id.notepad_content);

        title = title_Edittext.getText().toString();

        content = content_Edittext.getText().toString();

        updateNote(name , title , content , id);
        finish();

    }

    public void updateNote(String name , String title , String content , long id){
        ContentValues values = new ContentValues();
        values.put(FilesContract.FilesEntry.COLUMN_FILE_NAME , name);
        values.put(FilesContract.FilesEntry.COLUMN_TITLE , title);
        values.put(FilesContract.FilesEntry.COLUMN_CONTENT , content);

        String whereClause = String.format("%s IN (%d)" , FilesContract.FilesEntry._ID , id);

        int rows = getContentResolver().update(FilesContract.FilesEntry.CONTENT_URI , values , whereClause , null);

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_option_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()){
            case R.id.action_edit:
                edittextDialog();
                break;

            case R.id.action_save:
                if(getTitle().equals("Note") ){
                    saveDialog();
                }
                else {
                   // updateDialog(file_id);
                    updateNotewoDialog(file_id);
                }

                break;

        }


        return super.onOptionsItemSelected(item);
    }

    public void insertNote(String name , String title , String content){
        ContentValues values = new ContentValues();
        values.put(FilesContract.FilesEntry.COLUMN_FILE_NAME , name);
        values.put(FilesContract.FilesEntry.COLUMN_TITLE , title);
        values.put(FilesContract.FilesEntry.COLUMN_CONTENT , content);
        values.put(FilesContract.FilesEntry.COLUMN_IMG_URL , image_Url);
        values.put(FilesContract.FilesEntry.COLUMN_FOLDER_ID ,folderId);
        values.put(FilesContract.FilesEntry.COLUMN_ISNOTE , 1);

        Uri uri = getContentResolver().insert(FilesContract.FilesEntry.CONTENT_URI , values);


    }

    public void saveDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View root = inflater.inflate(R.layout.save_dialog , null);

        EditText title_Edittext = (EditText) findViewById(R.id.notepad_title);
        EditText content_Edittext = (EditText) findViewById(R.id.notepad_content);



        alertDialogBuilder.setTitle("Enter file name:");
        title = title_Edittext.getText().toString();

        content = content_Edittext.getText().toString();

        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EditText name_Edittext = root.findViewById(R.id.notepad_name);
                name = name_Edittext.getText().toString();
                Log.v("NAME" , "file name:" + name);
                insertNote(name , title , content);
                finish();

            }
        });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertDialogBuilder.setView(root);
        alertDialogBuilder.create().show();

    }

    public void edittextDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View root = inflater.inflate(R.layout.edit_notepad_text, null);
        gridView = (GridView) root.findViewById(R.id.gridview);
        GridviewAdapter gridviewAdapter = new GridviewAdapter(this , colorPicker);
        alertDialogBuilder.setView( root);
        gridView.setAdapter(gridviewAdapter);
        final EditText content_editText = (EditText) findViewById(R.id.notepad_content);

        SeekBar seekBar = root.findViewById(R.id.brush_size);
        int seekvalue = seekBar.getProgress();
        content_editText.setTextSize(seekvalue + 18);


        /**
         * Clicklistener on colors
         */
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                switch (i){
                    case 0:
                        content_editText.setTextColor(getResources().getColor(R.color.black_icon));
                        break;
                    case 1:
                        content_editText.setTextColor(getResources().getColor(R.color.blue_icon));
                        break;
                    case 2:
                        content_editText.setTextColor(getResources().getColor(R.color.gery_icon));
                        break;
                    case 3:
                        content_editText.setTextColor(getResources().getColor(R.color.green_icon));
                        break;
                    case 4:
                        content_editText.setTextColor(getResources().getColor(R.color.orange_icon));
                        break;
                    case 5:
                        content_editText.setTextColor(getResources().getColor(R.color.pink_icon));
                        break;
                    case 6:
                        content_editText.setTextColor(getResources().getColor(R.color.red_icon));
                        break;
                    case 7:
                        content_editText.setTextColor(getResources().getColor(R.color.light_blue_icon));
                        break;

                }

            }
        });
        /**
         * End of click listener
         */

        alertDialogBuilder.setTitle("Text Editor");

        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertDialogBuilder.create().show();

    }


    }

