package com.example.mohammadreza.sketch;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by mohammadreza on 8/25/2017.
 */

public class FilesAdapter extends CursorAdapter {

    private boolean isNote = true;

    public FilesAdapter(Context c , Cursor cursor , boolean note){
        super(c , cursor , 0);
        isNote = note;
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.list_files , viewGroup , false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView name =  view.findViewById(R.id.file_list_name);
        ImageView img = view.findViewById(R.id.file_image);

        String body = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        String isNote = cursor.getString(cursor.getColumnIndex("isNote"));
        int note = Integer.parseInt(isNote);
        name.setText(body);
        if(note == 1){
            img.setImageResource(R.drawable.ic_note);
        }
        else {
            img.setImageResource(R.drawable.ic_brush);
        }
    }
}
