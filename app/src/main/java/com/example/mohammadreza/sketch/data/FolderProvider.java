package com.example.mohammadreza.sketch.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by mohammadreza on 8/21/2017.
 */

public class FolderProvider extends ContentProvider {

    private static final int FOLDERS = 1000;
    private static final int FOLDERS_ID = 1001;
    private static final int FILES = 2000;
    private static final int FILES_ID = 2001;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {

        sUriMatcher.addURI(FolderContract.AUTHORITY, FolderContract.PATH_FOLDERS, FOLDERS);

        sUriMatcher.addURI(FolderContract.AUTHORITY, FolderContract.PATH_FOLDERS + "/#", FOLDERS_ID);

        sUriMatcher.addURI(FilesContract.AUTHORITY, FilesContract.PATH_FOLDERS, FILES);

        sUriMatcher.addURI(FilesContract.AUTHORITY, FilesContract.PATH_FOLDERS + "/#", FILES_ID);
    }


    private FolderDbHelper folderDbHelper;

    @Override
    public boolean onCreate() {
        folderDbHelper = new FolderDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs,
                        @Nullable String sortOrder) {
        SQLiteDatabase db = folderDbHelper.getReadableDatabase();

        Cursor cursor = null;

        int match = sUriMatcher.match(uri);

        switch (match) {
            case FOLDERS:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    cursor = db.query(FolderContract.FolderEntry.TABLE_NAME, projection, selection, selectionArgs,
                            null, null, sortOrder);
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }
                break;
            case FOLDERS_ID:
                selection = FolderContract.FolderEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(FolderContract.FolderEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                break;
            case FILES:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    cursor = db.query(FilesContract.FilesEntry.TABLE_NAME, projection, selection, selectionArgs,
                            null, null, sortOrder);
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }
                break;
            case FILES_ID:
                selection = FolderContract.FolderEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(FilesContract.FilesEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                break;
            default:
                throw new IllegalArgumentException("Cannot query this unknown Uri" + uri);
        }
        /**
         * vaqti az loader estefade mknim byd in method seda bznim
         * ta harvaqt chzi insert kardim khod b khod list update she
         * niaz nabashe dobare berim tu barname k bbinim add shde ya na
         */


        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {


        int match = sUriMatcher.match(uri);

        switch (match) {
            case FOLDERS:
                //getContext().getContentResolver().notifyChange(uri , null);
                return insertFolder(uri, contentValues);


            case FILES:
                //   getContext().getContentResolver().notifyChange(uri , null);
                return insertFile(uri, contentValues);


            default:
                throw new IllegalArgumentException("not supported" + uri);
        }

    }

    private Uri insertFolder(Uri uri, ContentValues values) {
        SQLiteDatabase db = folderDbHelper.getWritableDatabase();

        long id = db.insert(FolderContract.FolderEntry.TABLE_NAME, null, values);
        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, id);
    }

    public Uri insertFile(Uri uri, ContentValues contentValues) {

        SQLiteDatabase db = folderDbHelper.getWritableDatabase();
        long id = db.insert(FilesContract.FilesEntry.TABLE_NAME, null, contentValues);
        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, id);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = folderDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsdetected = -1;

        switch (match) {
            case FOLDERS:
                rowsdetected = db.delete(FolderContract.FolderEntry.TABLE_NAME, null, null);
                break;
            case FOLDERS_ID:
                selection = FolderContract.FolderEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsdetected = db.delete(FolderContract.FolderEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case FILES:
                rowsdetected = db.delete(FilesContract.FilesEntry.TABLE_NAME, null, null);
                break;
            case FILES_ID:
                selection = FilesContract.FilesEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsdetected = db.delete(FilesContract.FilesEntry.TABLE_NAME, selection, selectionArgs);
                break;
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return rowsdetected;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String selection, @Nullable String[] selectionArgs) {
        int match = sUriMatcher.match(uri);
        int rowsupdated = -1;
        switch (match) {
            case FOLDERS:

                break;
            case FOLDERS_ID:
                selection = FolderContract.FolderEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsupdated = updateFolder(uri , contentValues , selection , selectionArgs);
                break;
            case FILES:
                updateFile(uri, contentValues, selection, selectionArgs);
                break;
            case FILES_ID:
                selection = FilesContract.FilesEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsupdated = updateFile(uri , contentValues , selection , selectionArgs);
                break;

        }
        return rowsupdated;
    }

    public int updateFile(Uri uri, ContentValues values, String selection, String[] selectionArgs){

        SQLiteDatabase db = folderDbHelper.getWritableDatabase();

        int rowsupdated = db.update(FilesContract.FilesEntry.TABLE_NAME , values , selection , selectionArgs);

        return rowsupdated;
    }

    public int updateFolder(Uri uri, ContentValues values, String selection, String[] selectionArgs){

        SQLiteDatabase db = folderDbHelper.getWritableDatabase();

        int rowsupdated = db.update(FolderContract.FolderEntry.TABLE_NAME , values , selection , selectionArgs);

        return rowsupdated;
    }
}

