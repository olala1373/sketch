package com.example.mohammadreza.sketch;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.icu.util.Calendar;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mohammadreza on 8/22/2017.
 */


public class FoldersAdapter extends CursorAdapter {

    public FoldersAdapter(Context context , Cursor cursor){

        super(context , cursor , 0);
    }



    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {

        return LayoutInflater.from(context).inflate(R.layout.list_folders , viewGroup , false);
    }

    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView name =  view.findViewById(R.id.name_text);
        TextView time =  view.findViewById(R.id.time_text);

        String body = cursor.getString(cursor.getColumnIndexOrThrow("name"));

        Calendar ca = Calendar.getInstance();

        SimpleDateFormat  format = new SimpleDateFormat("dd/MM/yyyy");

        time.setText(format.format(ca.getTime()));

        name.setText(body);

    }
}
