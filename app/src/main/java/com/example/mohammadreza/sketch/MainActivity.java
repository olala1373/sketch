package com.example.mohammadreza.sketch;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


import com.example.mohammadreza.sketch.data.FolderContract.FolderEntry;

import java.util.Locale;

import static android.R.attr.typeface;


public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private static final int FOLDER_LOADER = 0;

    private FoldersAdapter foldersAdapter;

    String folderName ;
    ListView listView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list);
       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       // setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterNameDialog();
            }


        });

        displayData();

    }


    public void enterNameDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View root = inflater.inflate(R.layout.dialog_enter_folder_name, null);
        alertDialogBuilder.setView( root);



        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EditText editText = (EditText) root.findViewById(R.id.foldername_edittext);
                folderName = editText.getText().toString();
                if(!folderName.equals("")){
                    insertFolder(folderName);
                }



            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }

    public void insertFolder(String name){
      ContentValues values = new ContentValues();
        values.put(FolderEntry.COLUMN_FOLDER_NAME , name);

        Uri uri = getContentResolver().insert(FolderEntry.CONTENT_URI , values);

        Toast.makeText(this , uri.toString() , Toast.LENGTH_SHORT).show();
    }

    private void displayData(){


        String[] projection = {
                FolderEntry._ID ,
                FolderEntry.COLUMN_FOLDER_NAME
        };

        Cursor cursor = getContentResolver().query(
          FolderEntry.CONTENT_URI,
                projection,
                null,
                null,
                null
        );

        foldersAdapter = new FoldersAdapter(MainActivity.this , cursor);

        listView.setAdapter(foldersAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this , FilesActivity.class);

                Uri folderUri = ContentUris.withAppendedId(FolderEntry.CONTENT_URI, id);
                intent.setData(folderUri);
                //TODO pass the id and get the name in FilesActivity for title of Activitiy
                startActivity(intent);
            }
        });

        getSupportLoaderManager().initLoader(FOLDER_LOADER , null , MainActivity.this);


    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()){
            case R.id.action_delete:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

                alertDialogBuilder.setTitle("Are you Sure u want to delete all folders?");
                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int row = getContentResolver().delete(FolderEntry.CONTENT_URI , null , null);
                        Toast.makeText(MainActivity.this ,"" + row + "Folders deleted" , Toast.LENGTH_SHORT);
                    }
                });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this , "No folder deleted" , Toast.LENGTH_SHORT);
                    }
                });

                alertDialogBuilder.create().show();

               // displayData();
                return true;
            case R.id.action_settings:
                return true;

        }

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {
                FolderEntry._ID,
                FolderEntry.COLUMN_FOLDER_NAME,
               };

        return new CursorLoader(this,
                FolderEntry.CONTENT_URI,
                projection,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        foldersAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        foldersAdapter.swapCursor(null);
    }
}
